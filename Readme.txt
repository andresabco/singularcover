Design:
	Services:
	4 services, each one with its own database (except for eureka-naming-service):
	eureka-naming-service: Service for self-registration of the other services. Has no dependency over other services.
	film-service: Service that manages films and the operations over them. Has a dependency with eureka-naming-service.
	bonus-points-service: Service that manages bonus points operations over customers. Has a dependency with eureka-naming-service.
	rental-service: Service that manages rentals of films for customers. Has a dependency with the previous 3 services.
	
	Databases:
	For this exercise, I have used H2 databases are they require no instalation and are built on the fly. For a productive scenario, these would have to be replaced, but I thought that they were enough for the test.
	Each database consist in a single table, which contains minimal information for the test. In a real scenario, more fields would probably be needed.
	Films: Table that contains information regarding films. Accessible by film-service.
	Customers: Table that contains information regarding customers and their bonus points. Accessible by bonus-points-service.
	Rental: Table that contains information regarding rentals. It keeps ids from customers and films, but it is not directly linked to the previous tables. Accessible by rental-service.
	
	Balancing:
	For scalability purposes, automatic balancing has been set using Ribbon. We could instanciate multiple film and bonus-point services and, with the help of the eureka service, rental-service requests would be automatically balanced among the different instances. Note that for this, we would need to stop using the H2 database, as every instance would use its own database, which would lead to errors.
	
	Transactionality:
	Manual transactionality has been implemented in the rental-service, but that would need to be changed to use the Saga pattern. With all honesty, this has not been implemented because I am not familiar with that pattern yet, but I plan to investigate further to learn how to implement it properly. Unfortunately, I have already taken too long to be able to deliver for this test.
	
	Testing:
	Junit and mockito have been used for unit testing, and MockMvc for integration testing.
	
	Containers:
	Docker has been used as container for the different services.
	
	Programming language:
	The system has been coded using Java 11.
	
	Other frameworks:
	Spring boot, Swagger, Spring Data JPA.
	
How to run the program:
	1) Compilation: from the video-rental-store directory, run the following command in the console: mvn clean install -DskipTests
	2) Execution: once the docker virtual machine is up, run the following command in the console: docker-compose up
	3) Usage: access the URL of the service you want to use:
		a) Film service: http://localhost:8500/swagger-ui.html
		End points:
		- /films - Lists all films.
		- /films/{id} - Retrieves the information for the film with the specified ids.
		- /films/available - Lists all non-rented films.
		- /films/genre/{genre} - Lists all films of the specified genre.
		- /films/setFilmAvailable/{id} - Sets the film available.
		- /films/setFilmUnavailable/{id} - Sets the film unavailable and returns its type (0: new release, 1: regular film, 2: old film)
		
		b) Bonus points service: http://localhost:8600/swagger-ui.html
		End points:
		- /bonusPoints/add/film/{filmType}/customer/{customerId} - Adds the amount of bonus points corresponding to the film type to the specified customer.
		- /bonusPoints/customers - Lists all customers.
		- /bonusPoints/retrieve/customer/{customerId} - Retrieves the information for the customer with the specified id.
		
		c) Rental service: http://localhost:8700/swagger-ui.html
		End points:
		- /rentals - Lists all rentals.
		- /rentals/active - Lists all active rentals.
		- /rentals/rent/film/{filmId}/customer/{customerId}/days/{numberOfDays} - Rents the specified film for the specified customer the specified number of days.
		- /rentals/return/film/{filmId}/customer/{customerId}/days/{numberOfDaysHoldingTheFilm} - Returns the specified film for the specified customer. If the specified number of days is bigger than the number of days of renting paid upfront, the surcharge is calculated and returned in a message.
	
	Known problem: After testing in another computer to make sure that all worked as intended, the URLs provided did not work. The solution was to use the IP of the virtual machine running the containers instead of "localhost" (e.g.: http://192.168.99.100:8700/swagger-ui.html for the rental service).