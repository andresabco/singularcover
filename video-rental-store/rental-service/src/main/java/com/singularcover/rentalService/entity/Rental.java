package com.singularcover.rentalService.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "rental_type")
@Table(name = "rentals")
public class Rental {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected long id;

	@Column(name = "customer_id", nullable = false)
	protected long customerId;

	@Column(name = "film_id", nullable = false)
	protected long filmId;

	@Column(name = "number_of_days", nullable = false)
	protected int numberOfDays;

	@Column(nullable = false)
	protected boolean active = true;

	@Column(name = "price_per_day", nullable = false)
	protected int pricePerDay;

	@Transient
	protected int daysOfOffer;

	protected Rental(long customerId, long filmId, int numberOfDays, int pricePerday, int daysOfOffer) {
		this.customerId = customerId;
		this.filmId = filmId;
		this.numberOfDays = (daysOfOffer > numberOfDays) ? daysOfOffer : numberOfDays;
		this.pricePerDay = pricePerday;
		this.daysOfOffer = daysOfOffer;
	}

	public int getRentalPrice() {
		return pricePerDay * (1 + getDaysToPayAfterOffer());
	}

	private int getDaysToPayAfterOffer() {
		int extraDays = numberOfDays - daysOfOffer;
		return extraDays > 0 ? extraDays : 0;
	}

}
