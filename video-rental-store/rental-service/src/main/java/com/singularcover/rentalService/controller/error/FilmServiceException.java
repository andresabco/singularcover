package com.singularcover.rentalService.controller.error;

public class FilmServiceException extends RuntimeException {

	private static final long serialVersionUID = 2641892644885465431L;

	public FilmServiceException(String message) {
		super(message);
	}

}
