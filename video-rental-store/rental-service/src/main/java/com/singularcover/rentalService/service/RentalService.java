package com.singularcover.rentalService.service;

import java.util.List;

import com.singularcover.rentalService.entity.Rental;

public interface RentalService {

	List<Rental> findAll();

	List<Rental> findActive();

	String rentFilm(long customerId, long filmId, int numberOfDays);

	String returnFilm(long customerId, long filmId, int numberOfDaysHoldingTheFilm);

}
