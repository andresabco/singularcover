package com.singularcover.rentalService.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.singularcover.rentalService.controller.error.BonusPointsServiceException;
import com.singularcover.rentalService.controller.error.FilmServiceException;
import com.singularcover.rentalService.controller.error.RentalNotFoundException;
import com.singularcover.rentalService.entity.Rental;
import com.singularcover.rentalService.repository.RentalRepository;
import com.singularcover.rentalService.utils.Constants;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class RentalServiceImpl implements RentalService {

	private final FilmServiceProxy filmServiceProxy;

	private final BonusPointsServiceProxy bonusPointsServiceProxy;

	private final RentalRepository repository;

	private final RentalFactory rentalFactory;

	private Boolean setFilmUnavailableComplete = false;
	private Boolean createRentalComplete = false;
	private Boolean setFilmAvailableComplete = false;
	private Boolean finishRentalComplete = false;
	private long filmId = -1;
	private long customerId = -1;
	private int numberOfDays = -1;
	private int numberOfDaysHoldingTheFilm = -1;

	@Override
	public List<Rental> findAll() {
		return repository.findAll();
	}

	public List<Rental> findActive() {
		return repository.findByActiveTrue();
	}

	@Override
	@Transactional
	public String rentFilm(long customerId, long filmId, int numberOfDays) {
		setupTransactionValues(customerId, filmId, numberOfDays, numberOfDaysHoldingTheFilm);
		try {
			return rentFilmTransaction();
		} catch(RuntimeException e) {
			rollBackRentTransactionIfNeeded();
			throw e;
		}
	}

	@Override
	@Transactional
	public String returnFilm(long customerId, long filmId, int numberOfDaysHoldingTheFilm) {
		setupTransactionValues(customerId, filmId, numberOfDays, numberOfDaysHoldingTheFilm);
		try {
			return returnFilmTransaction();
		} catch(RuntimeException e) {
			rollBackReturnTransactionIfNeeded();
			throw e;
		}
	}

	private String rentFilmTransaction() {
		String filmType = setFilmUnavailable(filmId);
		if(filmType.equals(Constants.ERROR_CODE)) {
			String errorMessage = String.format(Constants.FILM_NOT_AVAILABLE, filmId);
			log.error(errorMessage);
			return errorMessage;
		}
		setFilmUnavailableComplete = true;
		Rental rental = createRental(customerId, filmId, filmType, numberOfDays);
		int newBonusPoints = addBonusPointsAndReturnUpdatedValue(filmType, customerId);
		return String.format(Constants.RENTAL_MESSAGE, filmId, numberOfDays, rental.getRentalPrice(), newBonusPoints);
	}

	private void setupTransactionValues(long customerId, long filmId, int numberOfDays,
			int numberOfDaysHoldingTheFilm) {
		this.customerId = customerId;
		this.filmId = filmId;
		this.numberOfDays = numberOfDays;
		this.numberOfDaysHoldingTheFilm = numberOfDaysHoldingTheFilm;
	}

	private String returnFilmTransaction() {
		setFilmAvailable(filmId);
		setFilmAvailableComplete = true;
		Rental rental = getFinishedRental(customerId, filmId);
		finishRentalComplete = true;
		return getReturnMessage(customerId, filmId, getSurcharge(rental, numberOfDaysHoldingTheFilm));
	}

	private String setFilmUnavailable(long filmId) {
		try {
			return filmServiceProxy.setFilmUnavailableAndGetType(filmId).getBody();
		} catch(RuntimeException e) {
			log.error(Constants.FILM_SERVICE_ERROR_MESSAGE);
			throw new FilmServiceException(e.getMessage());
		}
	}

	private int addBonusPointsAndReturnUpdatedValue(String filmType, long customerId) {
		try {
			return bonusPointsServiceProxy.addBonusPointsAndReturnUpdatedValue(filmType, customerId).getBody();
		} catch(RuntimeException e) {
			log.error(Constants.BONUS_POINTS_SERVICE_ERROR_MESSAGE);
			throw new BonusPointsServiceException(e.getMessage());
		}
	}

	private Rental createRental(long customerId, long filmId, String filmType, int numberOfDays) {
		Rental rental = rentalFactory.getRental(customerId, filmId, filmType, numberOfDays);
		repository.save(rental);
		createRentalComplete = true;
		return rental;
	}

	private Rental getRental(long customerId, long filmId) {
		return repository.findByCustomerIdAndFilmIdAndActiveTrue(customerId, filmId).orElseThrow(
				() -> new RentalNotFoundException(String.format(Constants.RENTAL_NOT_FOUND, customerId, filmId)));
	}

	private void rollBackRentTransactionIfNeeded() {
		rollbackFilmUnAvailableIfNeeded();
		rollbackCreateRentalIfNeeded();
	}

	private void rollbackFilmUnAvailableIfNeeded() {
		if(setFilmUnavailableComplete) {
			filmServiceProxy.setFilmAvailable(filmId);
		}
	}

	private void rollbackCreateRentalIfNeeded() {
		if(createRentalComplete) {
			deleteRentalByCustomerIdAndFilmId();
		}
	}

	private void deleteRentalByCustomerIdAndFilmId() {
		repository.deleteByCustomerIdAndFilmId(customerId, filmId);
		log.info(String.format(Constants.UNDO_RENTAL_MESSAGE, customerId, filmId));
	}

	private void setFilmAvailable(long filmId) {
		try {
			filmServiceProxy.setFilmAvailable(filmId);
		} catch(RuntimeException e) {
			log.error(Constants.FILM_SERVICE_ERROR_MESSAGE);
			throw new FilmServiceException(e.getMessage());
		}
	}

	private Rental getFinishedRental(long customerId, long filmId) {
		return finishRental(getRental(customerId, filmId));
	}

	private Rental finishRental(Rental rental) {
		rental.setActive(false);
		repository.save(rental);
		return rental;
	}

	private int getSurcharge(Rental rental, int numberOfDays) {
		return (numberOfDays - rental.getNumberOfDays()) * rental.getPricePerDay();
	}

	private String getReturnMessage(long customerId, long filmId, int surcharge) {
		StringBuilder returnMessage = new StringBuilder(
				String.format(Constants.RENTAL_RETURN_MESSAGE, customerId, filmId));
		if(surcharge > 0) {
			returnMessage.append(String.format(Constants.SURCHARGE_MESSAGE, surcharge));
		}
		return returnMessage.toString();
	}

	private void rollBackReturnTransactionIfNeeded() {
		rollbackFilmAvailableIfNeeded();
		rollbackRentalFinishedIfNeeded();
	}

	private void rollbackFilmAvailableIfNeeded() {
		if(setFilmAvailableComplete) {
			filmServiceProxy.setFilmUnavailableAndGetType(filmId);
		}
	}

	private void rollbackRentalFinishedIfNeeded() {
		if(finishRentalComplete) {
			setRentalActive();
		}
	}

	private void setRentalActive() {
		Rental rental = repository.findByCustomerIdAndFilmIdAndActiveFalse(customerId, filmId);
		rental.setActive(true);
		repository.save(rental);
	}

}
