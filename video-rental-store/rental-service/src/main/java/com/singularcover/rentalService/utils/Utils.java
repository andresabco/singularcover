package com.singularcover.rentalService.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class Utils {

	public static <T> ResponseEntity<T> createOkResponseEntity(T response) {
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
