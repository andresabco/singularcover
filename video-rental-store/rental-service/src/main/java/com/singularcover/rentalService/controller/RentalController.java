package com.singularcover.rentalService.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.singularcover.rentalService.entity.Rental;
import com.singularcover.rentalService.service.RentalService;
import com.singularcover.rentalService.utils.Utils;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class RentalController {

	private final RentalService service;

	@GetMapping("/rentals")
	public ResponseEntity<List<Rental>> findAll() {
		return Utils.createOkResponseEntity(service.findAll());
	}

	@GetMapping("/rentals/active")
	public ResponseEntity<List<Rental>> findActive() {
		return Utils.createOkResponseEntity(service.findActive());
	}

	@PostMapping("/rentals/rent/film/{filmId}/customer/{customerId}/days/{numberOfDays}")
	public ResponseEntity<String> rentFilm(@PathVariable long filmId, @PathVariable long customerId,
			@PathVariable int numberOfDays) {
		return Utils.createOkResponseEntity(service.rentFilm(customerId, filmId, numberOfDays));
	}

	@PostMapping("/rentals/return/film/{filmId}/customer/{customerId}/days/{numberOfDaysHoldingTheFilm}")
	public ResponseEntity<String> returnFilm(@PathVariable long filmId, @PathVariable long customerId,
			@PathVariable int numberOfDaysHoldingTheFilm) {
		return Utils.createOkResponseEntity(service.returnFilm(customerId, filmId, numberOfDaysHoldingTheFilm));
	}

}
