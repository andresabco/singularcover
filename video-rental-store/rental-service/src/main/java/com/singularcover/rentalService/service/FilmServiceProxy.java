package com.singularcover.rentalService.service;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "film-service")
@RibbonClient(name = "film-service")
public interface FilmServiceProxy {

	@GetMapping("/films/setFilmUnavailable/{filmId}")
	public ResponseEntity<String> setFilmUnavailableAndGetType(@PathVariable(name = "filmId") long filmId);

	@PostMapping("/films/setFilmAvailable/{filmId}")
	public ResponseEntity<Void> setFilmAvailable(@PathVariable(name = "filmId") long filmId);

}
