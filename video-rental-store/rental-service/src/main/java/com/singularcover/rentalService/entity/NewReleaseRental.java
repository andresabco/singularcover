package com.singularcover.rentalService.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.singularcover.rentalService.utils.Constants;

import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@DiscriminatorValue(Constants.NEW_RELEASE)
public class NewReleaseRental extends Rental {

	private static final int PRICE_PER_DAY = 3;
	private static final int DAYS_OF_OFFER = 1;

	public NewReleaseRental(long customerId, long filmId, int numberOfDays) {
		super(customerId, filmId, numberOfDays, PRICE_PER_DAY, DAYS_OF_OFFER);
	}

}