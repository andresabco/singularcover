package com.singularcover.rentalService.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.singularcover.rentalService.entity.Rental;

public interface RentalRepository extends JpaRepository<Rental, Long> {

	public Optional<Rental> findByCustomerIdAndFilmIdAndActiveTrue(long customerId, long filmId);

	public Rental findByCustomerIdAndFilmIdAndActiveFalse(long customerId, long filmId);

	public List<Rental> findByActiveTrue();

	public void deleteByCustomerIdAndFilmId(long customerId, long filmId);

}
