package com.singularcover.rentalService.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.singularcover.rentalService.utils.Constants;

import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@DiscriminatorValue(Constants.REGULAR_FILM)
public class RegularFilmRental extends Rental {

	private static final int PRICE_PER_DAY = 1;
	private static final int DAYS_OF_OFFER = 3;

	public RegularFilmRental(long customerId, long filmId, int numberOfDays) {
		super(customerId, filmId, numberOfDays, PRICE_PER_DAY, DAYS_OF_OFFER);
	}

}
