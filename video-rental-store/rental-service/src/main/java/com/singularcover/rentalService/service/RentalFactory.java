package com.singularcover.rentalService.service;

import com.singularcover.rentalService.entity.Rental;

public interface RentalFactory {

	Rental getRental(long customerId, long filmId, String filmType, int numberOfDays);

}
