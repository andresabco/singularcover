package com.singularcover.rentalService.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.singularcover.rentalService.utils.Constants;

import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@DiscriminatorValue(Constants.OLD_FILM)
public class OldFilmRental extends Rental {

	private static final int PRICE_PER_DAY = 1;
	private static final int DAYS_OF_OFFER = 5;

	public OldFilmRental(long customerId, long filmId, int numberOfDays) {
		super(customerId, filmId, numberOfDays, PRICE_PER_DAY, DAYS_OF_OFFER);
	}

}