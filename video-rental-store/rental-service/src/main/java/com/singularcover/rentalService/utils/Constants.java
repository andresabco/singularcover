package com.singularcover.rentalService.utils;

public class Constants {

	// Error constants
	public static final String ERROR_MESSAGE = "Error during Rest call.";
	public static final String RENTAL_NOT_FOUND = "Rental for customer %d and film %d not found.";
	public static final String FILM_NOT_AVAILABLE = "Film with id %d not available.";
	public static final String ERROR_CODE = "-1";
	public static final String FILM_SERVICE_ERROR_MESSAGE = "Error during call of Film Service Proxy.";
	public static final String BONUS_POINTS_SERVICE_ERROR_MESSAGE = "Error during call of Bonus Points Service Proxy.";
	public static final String UNDO_RENTAL_MESSAGE = "Undoing rental for customer %d and film %d";

	// Film type constants
	public static final String NEW_RELEASE = "0";
	public static final String REGULAR_FILM = "1";
	public static final String OLD_FILM = "2";
	public static final String FILM_TYPE_UNKNOWN = "Film type %s not recognized.";

	// Rental constants
	public static final String RENTAL_MESSAGE = "Film with id %d rented for %d days. Price: %d€. Bonus points accumulated for the customer: %d.";
	public static final String RENTAL_RETURN_MESSAGE = "Returned rental for customer %d and film %d. ";
	public static final String SURCHARGE_MESSAGE = "Film returned late. Surcharge: %d€.";

}
