package com.singularcover.rentalService.controller.error;

public class RentalNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 176031388739180680L;

	public RentalNotFoundException(String message) {
		super(message);
	}

}
