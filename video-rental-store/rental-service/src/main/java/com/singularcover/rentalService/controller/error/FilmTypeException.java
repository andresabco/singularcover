package com.singularcover.rentalService.controller.error;

public class FilmTypeException extends RuntimeException {

	private static final long serialVersionUID = -723766609410882082L;

	public FilmTypeException(String message) {
		super(message);
	}

}
