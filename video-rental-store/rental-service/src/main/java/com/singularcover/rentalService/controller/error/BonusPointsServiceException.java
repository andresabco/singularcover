package com.singularcover.rentalService.controller.error;

public class BonusPointsServiceException extends RuntimeException {

	private static final long serialVersionUID = -6377850501003996238L;

	public BonusPointsServiceException(String message) {
		super(message);
	}

}
