package com.singularcover.rentalService.service;

import org.springframework.stereotype.Service;

import com.singularcover.rentalService.controller.error.FilmTypeException;
import com.singularcover.rentalService.entity.NewReleaseRental;
import com.singularcover.rentalService.entity.OldFilmRental;
import com.singularcover.rentalService.entity.RegularFilmRental;
import com.singularcover.rentalService.entity.Rental;
import com.singularcover.rentalService.utils.Constants;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RentalFactoryImpl implements RentalFactory {

	@Override
	public Rental getRental(long customerId, long filmId, String filmType, int numberOfDays) {
		switch(filmType) {
			case Constants.NEW_RELEASE:
				return new NewReleaseRental(customerId, filmId, numberOfDays);
			case Constants.REGULAR_FILM:
				return new RegularFilmRental(customerId, filmId, numberOfDays);
			case Constants.OLD_FILM:
				return new OldFilmRental(customerId, filmId, numberOfDays);
		}
		String errorMessage = String.format(Constants.FILM_TYPE_UNKNOWN, filmType);
		log.error(errorMessage);
		throw new FilmTypeException(errorMessage);
	}

}
