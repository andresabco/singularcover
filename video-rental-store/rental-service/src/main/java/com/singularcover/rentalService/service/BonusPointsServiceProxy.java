package com.singularcover.rentalService.service;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "bonus-points-service")
@RibbonClient(name = "bonus-points-service")
public interface BonusPointsServiceProxy {

	@GetMapping("/bonusPoints/add/film/{filmType}/customer/{customerId}")
	public ResponseEntity<Integer> addBonusPointsAndReturnUpdatedValue(@PathVariable(name = "filmType") String filmType,
			@PathVariable(name = "customerId") long customerId);

}
