package com.singularcover.rentalService.service;

import static com.singularcover.rentalService.utils.Utils.createOkResponseEntity;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.singularcover.rentalService.controller.error.BonusPointsServiceException;
import com.singularcover.rentalService.controller.error.FilmServiceException;
import com.singularcover.rentalService.controller.error.RentalNotFoundException;
import com.singularcover.rentalService.entity.NewReleaseRental;
import com.singularcover.rentalService.entity.OldFilmRental;
import com.singularcover.rentalService.entity.RegularFilmRental;
import com.singularcover.rentalService.entity.Rental;
import com.singularcover.rentalService.repository.RentalRepository;
import com.singularcover.rentalService.utils.Constants;

@ExtendWith(MockitoExtension.class)
class RentalServiceImplTest {

	@Mock
	private FilmServiceProxy filmServiceProxy;

	@Mock
	private BonusPointsServiceProxy bonusPointsServiceProxy;

	@Mock
	private RentalRepository repository;

	@Mock
	private RentalFactory rentalFactory;

	@InjectMocks
	private RentalServiceImpl service;

	private long existingCustomerId = 1;
	private long nonExistingCustomerId = -1;
	private long existingFilmId = 1;
	private long nonExistingFilmId = -1;
	private int numberOfDays = 15;
	private int numberOfDaysHoldingTheFilm = 10;
	private int bonusPoints = 15;
	private Rental rentalWithoutSurcharge;
	private Rental rentalWithSurcharge;
	private Rental nonActiveRental;
	private List<Rental> rentalList;
	private List<Rental> activeRentalList;
	private List<Rental> emptyRentalList = List.of();

	@BeforeEach
	public void reloadValues() {
		rentalWithoutSurcharge = new RegularFilmRental(existingCustomerId, existingFilmId, numberOfDaysHoldingTheFilm);
		rentalWithSurcharge = new NewReleaseRental(existingCustomerId, existingFilmId, numberOfDaysHoldingTheFilm - 5);
		nonActiveRental = new OldFilmRental(existingCustomerId, existingFilmId, 20);
		nonActiveRental.setActive(false);
		activeRentalList = Stream.of(rentalWithoutSurcharge, rentalWithSurcharge).collect(Collectors.toList());
		rentalList = Stream.of(rentalWithoutSurcharge, rentalWithSurcharge, nonActiveRental)
				.collect(Collectors.toList());
	}

	@Test
	void whenFindAllAndRentalsExist_thenReturnRentalList() {
		// given
		given(repository.findAll()).willReturn(rentalList);
		// when
		List<Rental> result = service.findAll();
		// then
		assertEquals(rentalList, result);
		then(repository).should(times(1)).findAll();
	}

	@Test
	void whenFindAllAndRentalsDontExist_thenReturnEmptyList() {
		// given
		given(repository.findAll()).willReturn(emptyRentalList);
		// when
		List<Rental> result = service.findAll();
		// then
		assertEquals(emptyRentalList, result);
		then(repository).should(times(1)).findAll();
	}

	@Test
	void whenFindActive_thenReturnActiveRentalList() {
		// given
		given(repository.findByActiveTrue()).willReturn(activeRentalList);
		// when
		List<Rental> result = service.findActive();
		// then
		assertEquals(rentalList.stream().filter(rental -> rental.isActive()).collect(Collectors.toList()), result);
		then(repository).should(times(1)).findByActiveTrue();
	}

	@Test
	void whenRentAvailableFilm_ThenReturnResponseMessage() throws Exception {
		// given
		given(filmServiceProxy.setFilmUnavailableAndGetType(anyLong()))
				.willReturn(createOkResponseEntity(Constants.REGULAR_FILM));
		given(rentalFactory.getRental(anyLong(), anyLong(), anyString(), anyInt())).willReturn(rentalWithoutSurcharge);
		given(bonusPointsServiceProxy.addBonusPointsAndReturnUpdatedValue(anyString(), anyLong()))
				.willReturn(createOkResponseEntity(bonusPoints));
		// when
		String result = service.rentFilm(existingCustomerId, existingFilmId, numberOfDays);
		// then
		assertTrue(result.contains("Film with id " + existingFilmId + " rented for " + numberOfDays + " days"));
		then(filmServiceProxy).should(times(1)).setFilmUnavailableAndGetType(anyLong());
		then(rentalFactory).should(times(1)).getRental(anyLong(), anyLong(), anyString(), anyInt());
		then(bonusPointsServiceProxy).should(times(1)).addBonusPointsAndReturnUpdatedValue(anyString(), anyLong());
	}

	@Test
	void whenRentNonExistingFilm_thenThrowException() throws Exception {
		// given
		given(filmServiceProxy.setFilmUnavailableAndGetType(anyLong())).willThrow(RuntimeException.class);
		// then
		assertThrows(FilmServiceException.class,
				() -> service.rentFilm(existingCustomerId, nonExistingFilmId, numberOfDays));
		then(filmServiceProxy).should(times(1)).setFilmUnavailableAndGetType(anyLong());
	}

	@Test
	void whenRentNonAvailableFilm_thenReturnErrorMessage() throws Exception {
		// given
		given(filmServiceProxy.setFilmUnavailableAndGetType(anyLong()))
				.willReturn(new ResponseEntity<String>(Constants.ERROR_CODE, HttpStatus.NOT_ACCEPTABLE));
		// when
		String result = service.rentFilm(existingCustomerId, existingFilmId, numberOfDays);
		// then
		assertTrue(result.contains("not available"));
		then(filmServiceProxy).should(times(1)).setFilmUnavailableAndGetType(anyLong());
	}

	@Test
	void whenRentForNonExistingCustomer_thenThrowException() throws Exception {
		// given
		given(filmServiceProxy.setFilmUnavailableAndGetType(anyLong()))
				.willReturn(createOkResponseEntity(Constants.REGULAR_FILM));
		given(bonusPointsServiceProxy.addBonusPointsAndReturnUpdatedValue(anyString(), anyLong()))
				.willThrow(RuntimeException.class);
		// then
		assertThrows(BonusPointsServiceException.class,
				() -> service.rentFilm(existingCustomerId, nonExistingFilmId, numberOfDays));
		then(filmServiceProxy).should(times(1)).setFilmUnavailableAndGetType(anyLong());
		then(bonusPointsServiceProxy).should(times(1)).addBonusPointsAndReturnUpdatedValue(anyString(), anyLong());
	}

	@Test
	void whenReturnFilmWithoutSurcharge_thenGetResponseMessage() throws Exception {
		// given
		given(repository.findByCustomerIdAndFilmIdAndActiveTrue(anyLong(), anyLong()))
				.willReturn(Optional.of(rentalWithoutSurcharge));
		// when
		String result = service.returnFilm(existingCustomerId, existingFilmId, numberOfDaysHoldingTheFilm);
		// then
		assertFalse(result.contains("Surcharge"));
		then(repository).should(times(1)).findByCustomerIdAndFilmIdAndActiveTrue(anyLong(), anyLong());
		then(filmServiceProxy).should(times(1)).setFilmAvailable(anyLong());
	}

	@Test
	void whenReturnFilmWithSurcharge_thenGetResponseMessage() throws Exception {
		// given
		given(repository.findByCustomerIdAndFilmIdAndActiveTrue(anyLong(), anyLong()))
				.willReturn(Optional.of(rentalWithSurcharge));
		// when
		String result = service.returnFilm(existingCustomerId, existingFilmId, numberOfDaysHoldingTheFilm);
		// then
		assertTrue(result.contains("Surcharge"));
		then(repository).should(times(1)).findByCustomerIdAndFilmIdAndActiveTrue(anyLong(), anyLong());
		then(filmServiceProxy).should(times(1)).setFilmAvailable(anyLong());
	}

	@Test
	void whenReturnNonExistingFilm_thenThrowException() throws Exception {
		// given
		given(filmServiceProxy.setFilmAvailable(anyLong())).willThrow(FilmServiceException.class);
		// then
		assertThrows(FilmServiceException.class,
				() -> service.returnFilm(existingCustomerId, nonExistingFilmId, numberOfDaysHoldingTheFilm));
		then(filmServiceProxy).should(times(1)).setFilmAvailable(anyLong());
	}

	@Test
	void whenReturnNonExistingRentalForCustomer_thenThrowException() throws Exception {
		// given
		given(repository.findByCustomerIdAndFilmIdAndActiveTrue(anyLong(), anyLong())).willReturn(Optional.empty());
		// then
		assertThrows(RentalNotFoundException.class,
				() -> service.returnFilm(nonExistingCustomerId, existingFilmId, numberOfDaysHoldingTheFilm));
		then(repository).should(times(1)).findByCustomerIdAndFilmIdAndActiveTrue(anyLong(), anyLong());
	}

}
