
package com.singularcover.rentalService;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@SpringBootTest(classes = RentalServiceApplication.class)
class RentalServiceIntegrationTest {

	@Autowired
	private MockMvc mvc;

	@Test
	public void whenFindAll_thenGetStatus200() throws Exception {
		mvc.perform(get("/rentals").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].id", is(1)));
	}

	@Test
	public void whenFindActive_thenGetStatus200() throws Exception {
		mvc.perform(get("/rentals/active").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].active", is(true)));
	}

	@Test
	public void whenRentFilm_thenGetStatus200() throws Exception {
		long filmId = 1;
		long customerId = 3;
		int numberOfDays = 6;
		mvc.perform(post("/rentals/rent/film/" + filmId + "/customer/" + customerId + "/days/" + numberOfDays)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN))
				.andExpect(content().string(containsString("Film with id")));
	}

	@Test
	public void whenReturnFilm_thenGetStatus200() throws Exception {
		long filmId = 2;
		long customerId = 4;
		int numberOfDaysHoldingTheFilm = 20;
		mvc.perform(post(
				"/rentals/return/film/" + filmId + "/customer/" + customerId + "/days/" + numberOfDaysHoldingTheFilm)
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN))
				.andExpect(content().string(containsString("Returned rental for customer")));
	}

}
