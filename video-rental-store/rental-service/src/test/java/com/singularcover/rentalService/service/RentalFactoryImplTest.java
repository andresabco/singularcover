package com.singularcover.rentalService.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.singularcover.rentalService.controller.error.FilmTypeException;
import com.singularcover.rentalService.entity.NewReleaseRental;
import com.singularcover.rentalService.entity.OldFilmRental;
import com.singularcover.rentalService.entity.RegularFilmRental;
import com.singularcover.rentalService.entity.Rental;
import com.singularcover.rentalService.utils.Constants;

@ExtendWith(SpringExtension.class)
class RentalFactoryImplTest {

	public RentalFactoryImplTest() {
		this.rentalFactory = new RentalFactoryImpl();
	}

	private final RentalFactory rentalFactory;

	private static long customerId = 1;
	private static long filmId = 1;
	private static String notValidFilmType = "-1";
	private static int numberOfDays = 1;

	@Test
	void whenGetRentalForNewReleaseFilm_thenReturnNewReleaseRental() {
		// when
		Rental result = rentalFactory.getRental(customerId, filmId, Constants.NEW_RELEASE, numberOfDays);
		// then
		assertTrue(result instanceof NewReleaseRental);
	}

	@Test
	void whenGetRentalForRegularFilm_thenReturnRegularRental() {
		// when
		Rental result = rentalFactory.getRental(customerId, filmId, Constants.REGULAR_FILM, numberOfDays);
		// then
		assertTrue(result instanceof RegularFilmRental);
	}

	@Test
	void whenGetRentalForOldFilm_thenReturnOldRental() {
		// when
		Rental result = rentalFactory.getRental(customerId, filmId, Constants.OLD_FILM, numberOfDays);
		// then
		assertTrue(result instanceof OldFilmRental);
	}

	@Test
	void whenGetRentalForNotValidFilmType_thenThrowException() {
		assertThrows(FilmTypeException.class,
				() -> rentalFactory.getRental(customerId, filmId, notValidFilmType, numberOfDays));
	}

}
