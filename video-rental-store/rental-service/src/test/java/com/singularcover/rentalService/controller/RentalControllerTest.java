package com.singularcover.rentalService.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.singularcover.rentalService.entity.NewReleaseRental;
import com.singularcover.rentalService.entity.OldFilmRental;
import com.singularcover.rentalService.entity.RegularFilmRental;
import com.singularcover.rentalService.entity.Rental;
import com.singularcover.rentalService.service.RentalServiceImpl;

@ExtendWith(MockitoExtension.class)
class RentalControllerTest {

	@Mock
	private RentalServiceImpl service;

	@InjectMocks
	private RentalController controller;

	private static long customerId = 1;
	private static long filmId = 1;
	private static int numberOfDays = 15;
	private static Rental newReleaseRental = new NewReleaseRental(customerId, filmId, numberOfDays - 5);
	private static Rental regularRental = new RegularFilmRental(customerId, filmId, numberOfDays);
	private static Rental oldRental = new OldFilmRental(customerId, filmId, 20);
	private static List<Rental> rentalList = Stream.of(newReleaseRental, regularRental, oldRental)
			.collect(Collectors.toList());
	private List<Rental> activeRentalList = Stream.of(newReleaseRental, regularRental).collect(Collectors.toList());
	private String responseMessage = "responseMessage";

	@BeforeAll
	public static void initValues() {
		oldRental.setActive(false);
	}

	@Test
	void whenFindAll_thenReturnRentalList() {
		// given
		given(service.findAll()).willReturn(rentalList);
		// when
		List<Rental> result = controller.findAll().getBody();
		// then
		assertEquals(rentalList, result);
		then(service).should(times(1)).findAll();
	}

	@Test
	void whenFindActive_thenReturnRentalList() {
		// given
		given(service.findActive()).willReturn(activeRentalList);
		// when
		List<Rental> result = controller.findActive().getBody();
		// then
		assertEquals(rentalList.stream().filter(rental -> rental.isActive()).collect(Collectors.toList()), result);
		assertTrue(result.get(0).isActive());
		then(service).should(times(1)).findActive();
	}

	@Test
	void whenRentFilm_thenReturnResponseMessage() {
		// given
		given(service.rentFilm(anyLong(), anyLong(), anyInt())).willReturn(responseMessage);
		// when
		String result = controller.rentFilm(filmId, customerId, numberOfDays).getBody();
		// then
		assertEquals(responseMessage, result);
		then(service).should(times(1)).rentFilm(anyLong(), anyLong(), anyInt());
	}

	@Test
	void whenReturnFilm_thenReturnResponseMessage() {
		// given
		given(service.returnFilm(anyLong(), anyLong(), anyInt())).willReturn(responseMessage);
		// when
		String result = controller.returnFilm(filmId, customerId, numberOfDays).getBody();
		// then
		assertEquals(responseMessage, result);
		then(service).should(times(1)).returnFilm(anyLong(), anyLong(), anyInt());
	}

}
