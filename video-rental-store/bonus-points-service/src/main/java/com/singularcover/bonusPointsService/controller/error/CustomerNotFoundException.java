package com.singularcover.bonusPointsService.controller.error;

public class CustomerNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 8629585491803668629L;

	public CustomerNotFoundException(String message) {
		super(message);
	}

}
