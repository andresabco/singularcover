package com.singularcover.bonusPointsService.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.singularcover.bonusPointsService.controller.error.CustomerNotFoundException;
import com.singularcover.bonusPointsService.entity.Customer;
import com.singularcover.bonusPointsService.repository.CustomerRepository;
import com.singularcover.bonusPointsService.utils.Constants;
import com.singularcover.bonusPointsService.utils.FilmType;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class BonusPointsServiceImpl implements BonusPointsService {

	private final CustomerRepository repository;

	@Override
	public List<Customer> findAll() {
		return repository.findAll();
	}

	@Override
	public Integer addBonusPointsAndReturnUpdatedValue(String filmType, long customerId) {
		return addBonusPointsAndSave(getCustomerById(customerId), FilmType.getBonusPointsPerFilmType(filmType));
	}

	@Override
	public Integer getBonusPointsFromCustomer(long customerId) {
		return getCustomerById(customerId).getBonusPoints();
	}

	private Customer getCustomerById(long customerId) {
		return repository.findById(customerId).orElseThrow(
				() -> new CustomerNotFoundException(String.format(Constants.CUSTOMER_NOT_FOUND, customerId)));
	}

	private int addBonusPointsAndSave(Customer customer, int bonusPoints) {
		setNewBonusPointsValue(customer, bonusPoints);
		return customer.getBonusPoints();
	}

	private void setNewBonusPointsValue(Customer customer, int bonusPoints) {
		customer.setBonusPoints(bonusPoints + customer.getBonusPoints());
		repository.save(customer);
	}

}
