package com.singularcover.bonusPointsService.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.singularcover.bonusPointsService.entity.Customer;
import com.singularcover.bonusPointsService.service.BonusPointsService;
import com.singularcover.bonusPointsService.utils.Utils;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class BonusPointsController {

	private final BonusPointsService service;

	@GetMapping("/bonusPoints/customers")
	public ResponseEntity<List<Customer>> findAll() {
		return Utils.createOkResponseEntity(service.findAll());
	}

	@GetMapping("/bonusPoints/add/film/{filmType}/customer/{customerId}")
	public ResponseEntity<Integer> addBonusPointsAndReturnUpdatedValue(@PathVariable String filmType,
			@PathVariable long customerId) {
		return Utils.createOkResponseEntity(service.addBonusPointsAndReturnUpdatedValue(filmType, customerId));
	}

	@GetMapping("/bonusPoints/retrieve/customer/{customerId}")
	public ResponseEntity<Integer> getBonusPointsFromCustomer(@PathVariable long customerId) {
		return Utils.createOkResponseEntity(service.getBonusPointsFromCustomer(customerId));
	}

}
