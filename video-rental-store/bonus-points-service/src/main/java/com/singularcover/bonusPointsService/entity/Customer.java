package com.singularcover.bonusPointsService.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@Table(name = "customers")
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(length = 50, nullable = false)
	private String name;

	@Column(name = "bonus_points", nullable = false)
	private int bonusPoints;

	public Customer(String name, int bonusPoints) {
		this.name = name;
		this.bonusPoints = bonusPoints;
	}

}
