package com.singularcover.bonusPointsService.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.singularcover.bonusPointsService.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {}
