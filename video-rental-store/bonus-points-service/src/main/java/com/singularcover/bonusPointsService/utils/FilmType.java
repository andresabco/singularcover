package com.singularcover.bonusPointsService.utils;

public enum FilmType {

	NEW_RELEASE("0", 2), REGULAR("1", 1), OLD("2", 1);

	private String filmType;
	private int bonusPoints;

	FilmType(String filmType, int bonusPoints) {
		this.filmType = filmType;
		this.bonusPoints = bonusPoints;
	}

	public static int getBonusPointsPerFilmType(String filmType) {
		for(FilmType f : values()) {
			if(f.filmType.equals(filmType)) {
				return f.getBonusPoints();
			}
		}
		return 0;
	}

	public int getBonusPoints() {
		return bonusPoints;
	}

	public String getFilmType() {
		return filmType;
	}

}
