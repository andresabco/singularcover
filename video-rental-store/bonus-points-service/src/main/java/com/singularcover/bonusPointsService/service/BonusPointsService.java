package com.singularcover.bonusPointsService.service;

import java.util.List;

import com.singularcover.bonusPointsService.entity.Customer;

public interface BonusPointsService {

	List<Customer> findAll();

	Integer addBonusPointsAndReturnUpdatedValue(String filmType, long customerId);

	Integer getBonusPointsFromCustomer(long customerId);

}
