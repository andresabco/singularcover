package com.singularcover.bonusPointsService.utils;

public class Constants {

	// Error constants
	public static final String ERROR_MESSAGE = "Error during Rest call.";
	public static final String CUSTOMER_NOT_FOUND = "Customer with id %d not found.";

}
