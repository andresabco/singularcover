package com.singularcover.bonusPointsService.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.singularcover.bonusPointsService.entity.Customer;
import com.singularcover.bonusPointsService.service.BonusPointsServiceImpl;

@ExtendWith(MockitoExtension.class)
class BonusPointsControllerTest {

	@Mock
	private BonusPointsServiceImpl service;

	@InjectMocks
	private BonusPointsController controller;

	private int bonusPoints = 10;
	private String filmType = "1";
	private long customerId = 1;
	private Customer customer1 = new Customer("Name1", 5);
	private Customer customer2 = new Customer("Name2", 0);
	private Customer customer3 = new Customer("Name3", 15);
	private List<Customer> customerList = Stream.of(customer1, customer2, customer3).collect(Collectors.toList());

	@Test
	void whenFindAll_thenReturnCustomerList() {
		// given
		given(service.findAll()).willReturn(customerList);
		// when
		List<Customer> result = controller.findAll().getBody();
		// then
		assertEquals(customerList, result);
		then(service).should(times(1)).findAll();
	}

	@Test
	void whenAddBonusPointsToCustomer_thenReturnNewBonusValue() {
		// given
		given(service.addBonusPointsAndReturnUpdatedValue(anyString(), anyLong())).willReturn(bonusPoints);
		// when
		int result = controller.addBonusPointsAndReturnUpdatedValue(filmType, customerId).getBody();
		// then
		assertEquals(bonusPoints, result);
		then(service).should(times(1)).addBonusPointsAndReturnUpdatedValue(anyString(), anyLong());
	}

	@Test
	void whenGetBonusPointsFromCustomer_thenReturnBonusPoints() {
		// given
		given(service.getBonusPointsFromCustomer(anyLong())).willReturn(bonusPoints);
		// when
		int result = controller.getBonusPointsFromCustomer(customerId).getBody();
		// then
		assertEquals(bonusPoints, result);
		then(service).should(times(1)).getBonusPointsFromCustomer(anyLong());
	}

}
