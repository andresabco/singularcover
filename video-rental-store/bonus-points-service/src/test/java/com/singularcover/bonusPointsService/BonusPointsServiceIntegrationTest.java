
package com.singularcover.bonusPointsService;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@SpringBootTest(classes = BonusPointsServiceApplication.class)
class BonusPointsServiceIntegrationTest {

	@Autowired
	private MockMvc mvc;

	@Test
	public void whenFindAll_thenGetStatus200() throws Exception {
		mvc.perform(get("/bonusPoints/customers").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].name", is("John"))).andExpect(jsonPath("$", hasSize(4)));
	}

	@Test
	public void whenAddBonusPointsAndReturnUpdatedValue_thenGetStatus200() throws Exception {
		String filmType = "2";
		int customerId = 3;
		String newBonusPoints = "1";
		mvc.perform(get("/bonusPoints/add/film/" + filmType + "/customer/" + customerId)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(content().string(equalTo(newBonusPoints)));
	}

	@Test
	public void whenGetBonusPointsFromCustomer_thenGetStatus200() throws Exception {
		int customerId = 1;
		String bonusPoints = "25";
		mvc.perform(get("/bonusPoints/retrieve/customer/" + customerId).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(content().string(equalTo(bonusPoints)));
	}

}
