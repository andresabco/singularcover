package com.singularcover.bonusPointsService.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.singularcover.bonusPointsService.controller.error.CustomerNotFoundException;
import com.singularcover.bonusPointsService.entity.Customer;
import com.singularcover.bonusPointsService.repository.CustomerRepository;
import com.singularcover.bonusPointsService.utils.FilmType;

@ExtendWith(MockitoExtension.class)
class BonusPointsServiceImplTest {

	@Mock
	private CustomerRepository repository;

	@InjectMocks
	private BonusPointsServiceImpl service;

	private String newReleaseType = FilmType.NEW_RELEASE.getFilmType();
	private String invalidFilmType = "-1";
	private Customer customer10bps;
	private List<Customer> customerList = Stream.of(customer10bps).collect(Collectors.toList());

	@BeforeEach
	public void reloadValues() {
		customer10bps = new Customer("name1", 10);
	}

	@Test
	void whenFindAll_thenReturnCustomerList() {
		// given
		given(service.findAll()).willReturn(customerList);
		// when
		List<Customer> result = repository.findAll();
		// then
		assertEquals(customerList, result);
		then(repository).should(times(1)).findAll();
	}

	@Test
	void whenAddBonusPointsToExistingCustomer_thenReturnNewBonusValue() {
		// given
		given(repository.findById(anyLong())).willReturn(Optional.of(customer10bps));
		// when
		int result = service.addBonusPointsAndReturnUpdatedValue(newReleaseType, 1);
		// then
		assertEquals(12, result);
		then(repository).should(times(1)).findById(anyLong());
	}

	@Test
	void whenAddBonusPointsToExistingCustomerAndInvalidFilmType_thenReturnSameBonusValue() {
		// given
		given(repository.findById(anyLong())).willReturn(Optional.of(customer10bps));
		// when
		int result = service.addBonusPointsAndReturnUpdatedValue(invalidFilmType, 1);
		// then
		assertEquals(10, result);
		then(repository).should(times(1)).findById(anyLong());
	}

	@Test
	void whenAddBonusPointsToNonExistingCustomer_thenThrowException() {
		// given
		given(repository.findById(anyLong())).willReturn(Optional.empty());
		// then
		assertThrows(CustomerNotFoundException.class,
				() -> service.addBonusPointsAndReturnUpdatedValue(newReleaseType, 1));
		then(repository).should(times(1)).findById(anyLong());
	}

	@Test
	void whenGetBonusPointsFromExistingCustomer_thenReturnCurrentBonusPoints() {
		// given
		given(repository.findById(anyLong())).willReturn(Optional.of(customer10bps));
		// when
		int result = service.getBonusPointsFromCustomer(1);
		// then
		assertEquals(10, result);
		then(repository).should(times(1)).findById(anyLong());
	}

	@Test
	void whenGetBonusPointsFromNonExistingCustomer_thenThrowException() {
		// given
		given(repository.findById(anyLong())).willReturn(Optional.empty());
		// then
		assertThrows(CustomerNotFoundException.class, () -> service.getBonusPointsFromCustomer(1));
		then(repository).should(times(1)).findById(anyLong());
	}

}
