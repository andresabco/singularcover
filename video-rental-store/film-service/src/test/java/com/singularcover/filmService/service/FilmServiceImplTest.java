package com.singularcover.filmService.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.singularcover.filmService.controller.error.FilmNotFoundException;
import com.singularcover.filmService.entity.Film;
import com.singularcover.filmService.repository.FilmRepository;
import com.singularcover.filmService.utils.Constants;
import com.singularcover.filmService.utils.FilmType;

@ExtendWith(MockitoExtension.class)
class FilmServiceImplTest {

	@Mock
	private FilmRepository repository;

	@InjectMocks
	private FilmServiceImpl service;

	private String horrorGenre = "horror";
	private String adventureGenre = "adventure";
	private String comdeyGenre = "comedy";
	private Film filmAdventure;
	private Film filmComedy;
	private Film filmHorrorAvailable;
	private Film filmHorrorNotAvailable;
	private List<Film> emptyFilmList = List.of();
	private List<Film> fullFilmList;
	private List<Film> horrorFilmList;
	private List<Film> availableFilms;

	@BeforeEach
	public void reloadValues() {
		filmAdventure = new Film("title1", adventureGenre, FilmType.OLD.getFilmType(), true);
		filmComedy = new Film("title2", comdeyGenre, FilmType.NEW_RELEASE.getFilmType(), false);
		filmHorrorAvailable = new Film("title4", horrorGenre, FilmType.NEW_RELEASE.getFilmType(), true);
		filmHorrorNotAvailable = new Film("title3", horrorGenre, FilmType.REGULAR.getFilmType(), false);
		fullFilmList = Stream.of(filmAdventure, filmComedy, filmHorrorAvailable, filmHorrorNotAvailable)
				.collect(Collectors.toList());
		horrorFilmList = Stream.of(filmHorrorAvailable, filmHorrorNotAvailable).collect(Collectors.toList());
		availableFilms = Stream.of(filmAdventure, filmHorrorAvailable).collect(Collectors.toList());
	}

	@Test
	void whenFindAllAndFilmsExist_thenReturnFilmList() {
		// given
		given(repository.findAll()).willReturn(fullFilmList);
		// when
		List<Film> result = service.findAll();
		// then
		assertEquals(fullFilmList, result);
		then(repository).should(times(1)).findAll();
	}

	@Test
	void whenFindAllAndFilmsDontExist_thenReturnEmptyList() {
		// given
		given(repository.findAll()).willReturn(emptyFilmList);
		// when
		List<Film> result = service.findAll();
		// then
		assertEquals(emptyFilmList, result);
		then(repository).should(times(1)).findAll();
	}

	@Test
	void whenFindAvailable_thenReturnListOfAvailableFilms() {
		// given
		given(repository.findByAvailableTrue()).willReturn(availableFilms);
		// when
		List<Film> result = service.findAvailable();
		// then
		assertEquals(fullFilmList.stream().filter(film -> film.isAvailable()).collect(Collectors.toList()), result);
		assertEquals(result.get(0).isAvailable(), true);
		then(repository).should(times(1)).findByAvailableTrue();
	}

	@Test
	void whenFindByGenre_thenReturnListWithSpecifiedType() {
		// given
		given(repository.findByGenre(anyString())).willReturn(horrorFilmList);
		// when
		List<Film> result = service.findByGenre(horrorGenre);
		// then
		assertEquals(horrorFilmList, result);
		assertEquals(horrorGenre, result.get(0).getGenre());
		then(repository).should(times(1)).findByGenre(anyString());
	}

	@Test
	void whenFindByExistingId_thenReturnFilm() {
		// given
		given(repository.findById(anyLong())).willReturn(Optional.of(filmAdventure));
		// when
		Film result = service.findById(1);
		// then
		assertEquals(filmAdventure, result);
		then(repository).should(times(1)).findById(anyLong());
	}

	@Test
	void whenFindByNotExistingId_thenThrowException() {
		// given
		given(repository.findById(anyLong())).willReturn(Optional.empty());
		// then
		assertThrows(FilmNotFoundException.class, () -> service.findById(1));
		then(repository).should(times(1)).findById(anyLong());
	}

	@Test
	void whenRentAvailableFilm_thenReturnFilm() {
		// given
		given(repository.findById(anyLong())).willReturn(Optional.of(filmHorrorAvailable));
		// when
		String result = service.setFilmUnavailableAndGetType(filmHorrorAvailable.getId());
		// then
		assertEquals(filmHorrorAvailable.getType(), result);
		then(repository).should(times(1)).findById(anyLong());
		then(repository).should(times(1)).save(any(Film.class));
	}

	@Test
	void whenRentNotAvailableFilm_thenReturnErrorCode() {
		// given
		given(repository.findById(anyLong())).willReturn(Optional.of(filmHorrorNotAvailable));
		// when
		String result = service.setFilmUnavailableAndGetType(filmHorrorNotAvailable.getId());
		// then
		assertEquals(Constants.ERROR_CODE, result);
		then(repository).should(times(1)).findById(anyLong());
		then(repository).should(times(0)).save(any(Film.class));
	}

	@Test
	void whenReturnFilm_thenVerifyRepositoryOperations() {
		// given
		given(repository.findById(anyLong())).willReturn(Optional.of(filmComedy));
		// when
		service.setFilmAvailable(filmComedy.getId());
		// then
		then(repository).should(times(1)).findById(anyLong());
		then(repository).should(times(1)).save(any(Film.class));
	}

}
