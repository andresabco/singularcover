package com.singularcover.filmService.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.singularcover.filmService.entity.Film;
import com.singularcover.filmService.service.FilmService;
import com.singularcover.filmService.utils.FilmType;

@ExtendWith(MockitoExtension.class)
class FilmControllerTest {

	@Mock
	private FilmService service;

	@InjectMocks
	private FilmController controller;

	private String horrorGenre = "horror";
	private String adventureGenre = "adventure";
	private long filmId = 1;
	private Film filmAdventure = new Film("title1", adventureGenre, FilmType.OLD.getFilmType(), true);
	private Film filmHorrorAvailable = new Film("title2", horrorGenre, FilmType.NEW_RELEASE.getFilmType(), true);
	private Film filmHorrorUnavailable = new Film("title3", horrorGenre, FilmType.REGULAR.getFilmType(), false);
	private List<Film> fullFilmList = Stream.of(filmAdventure, filmHorrorAvailable, filmHorrorUnavailable)
			.collect(Collectors.toList());
	private List<Film> horrorFilmList = Stream.of(filmHorrorAvailable, filmHorrorUnavailable)
			.collect(Collectors.toList());
	private List<Film> availableFilms = Stream.of(filmAdventure, filmHorrorAvailable).collect(Collectors.toList());

	@Test
	void whenFindAll_thenReturnFilmList() {
		// given
		given(service.findAll()).willReturn(fullFilmList);
		// when
		List<Film> result = controller.findAll().getBody();
		// then
		assertEquals(fullFilmList, result);
		then(service).should(times(1)).findAll();
	}

	@Test
	void whenFindAvailable_thenReturnListOfAvailableFilms() {
		// given
		given(service.findAvailable()).willReturn(availableFilms);
		// when
		List<Film> result = controller.findAvailable().getBody();
		// then
		assertEquals(fullFilmList.stream().filter(film -> film.isAvailable()).collect(Collectors.toList()), result);
		assertEquals(result.get(0).isAvailable(), true);
		then(service).should(times(1)).findAvailable();
	}

	@Test
	void whenFindByGenre_thenReturnListWithSpecifiedType() {
		// given
		given(service.findByGenre(anyString())).willReturn(horrorFilmList);
		// when
		List<Film> result = controller.findByGenre(horrorGenre).getBody();
		// then
		assertEquals(
				fullFilmList.stream().filter(film -> film.getGenre().equals(horrorGenre)).collect(Collectors.toList()),
				result);
		assertEquals(result.get(0).getGenre(), horrorGenre);
		then(service).should(times(1)).findByGenre(anyString());
	}

	@Test
	void whenFindByExistingId_thenReturnFilm() {
		// given
		given(service.findById(anyLong())).willReturn(filmAdventure);
		// when
		Film result = controller.findById(filmId).getBody();
		// then
		assertEquals(filmAdventure, result);
		then(service).should(times(1)).findById(anyLong());
	}

	@Test
	void whenSetFilmUnavailableAndGetType_thenReturnFilmType() {
		// given
		given(service.setFilmUnavailableAndGetType(anyLong())).willReturn(filmHorrorUnavailable.getType());
		// when
		String result = controller.setFilmUnavailableAndGetType(filmId).getBody();
		// then
		assertEquals(filmHorrorUnavailable.getType(), result);
		then(service).should(times(1)).setFilmUnavailableAndGetType(anyLong());
	}

	@Test
	void whensetFilmAvailable_thenVerifyServiceOperation() {
		// when
		controller.setFilmAvailable(filmId).getBody();
		// then
		then(service).should(times(1)).setFilmAvailable(anyLong());
	}

}
