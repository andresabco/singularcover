package com.singularcover.filmService;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@SpringBootTest(classes = FilmServiceApplication.class)
class FilmServiceIntegrationTest {

	@Autowired
	private MockMvc mvc;

	@Test
	public void whenFindAll_thenGetStatus200() throws Exception {
		mvc.perform(get("/films").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].title", is("title1"))).andExpect(jsonPath("$", hasSize(3)));
	}

	@Test
	public void whenFindAvailable_thenGetStatus200() throws Exception {
		mvc.perform(get("/films/available").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].available", is(true))).andExpect(jsonPath("$", hasSize(2)));
	}

	@Test
	public void whenFindByGenre_thenGetStatus200() throws Exception {
		String genre = "horror";
		mvc.perform(get("/films/genre/" + genre).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].genre", is(genre))).andExpect(jsonPath("$", hasSize(2)));
	}

	@Test
	public void whenFindById_thenGetStatus200() throws Exception {
		int id = 1;
		mvc.perform(get("/films/" + id).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.id", is(id)));
	}

	@Test
	public void whenSetFilmUnavailable_thenGetStatus200() throws Exception {
		int id = 1;
		String type = "1";
		mvc.perform(get("/films/setFilmUnavailable/" + id).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN))
				.andExpect(content().string(equalTo(type)));
	}

	@Test
	public void whenSetFilmAvailable_thenGetStatus200() throws Exception {
		int id = 2;
		mvc.perform(post("/films/setFilmAvailable/" + id).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

}
