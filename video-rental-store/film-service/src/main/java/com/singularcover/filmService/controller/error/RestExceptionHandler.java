package com.singularcover.filmService.controller.error;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.singularcover.filmService.utils.Constants;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler({ Exception.class })
	@ResponseBody
	public ResponseEntity<RestError> handleAnyException(Exception e) {
		return createError(e, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler({ FilmNotFoundException.class })
	@ResponseBody
	public ResponseEntity<RestError> handleCustomerNotFoundException(FilmNotFoundException e) {
		return createError(e, HttpStatus.NOT_FOUND);
	}

	private ResponseEntity<RestError> createError(Throwable t, HttpStatus status) {
		log.error(Constants.ERROR_MESSAGE, t);
		return new ResponseEntity<>(new RestError(t), new HttpHeaders(), status);
	}

}
