package com.singularcover.filmService.controller.error;

public class FilmNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1844326295352671643L;

	public FilmNotFoundException(String message) {
		super(message);
	}

}
