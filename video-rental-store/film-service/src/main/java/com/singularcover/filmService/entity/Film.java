package com.singularcover.filmService.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@Table(name = "films")
public class Film {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(nullable = false, length = 120)
	private String title;

	@Column(nullable = false, length = 50)
	private String genre;

	@Column(nullable = false, length = 1)
	private String type;

	@Column(nullable = false)
	private boolean available;

	public Film(String title, String genre, String type, boolean available) {
		this.title = title;
		this.genre = genre;
		this.type = type;
		this.available = available;
	}

}
