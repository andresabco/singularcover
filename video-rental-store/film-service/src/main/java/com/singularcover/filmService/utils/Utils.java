package com.singularcover.filmService.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class Utils {

	public static <T> ResponseEntity<T> createOkResponseEntity(T response) {
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	public static ResponseEntity<Void> createEmptyOkResponseEntity() {
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
