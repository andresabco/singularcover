package com.singularcover.filmService.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.singularcover.filmService.controller.error.FilmNotFoundException;
import com.singularcover.filmService.entity.Film;
import com.singularcover.filmService.repository.FilmRepository;
import com.singularcover.filmService.utils.Constants;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor
public class FilmServiceImpl implements FilmService {

	private final FilmRepository repository;

	@Override
	public List<Film> findAll() {
		return repository.findAll();
	}

	@Override
	public List<Film> findAvailable() {
		return repository.findByAvailableTrue();
	}

	@Override
	public List<Film> findByGenre(String genre) {
		return repository.findByGenre(genre);
	}

	@Override
	public Film findById(long id) {
		return repository.findById(id)
				.orElseThrow(() -> new FilmNotFoundException(String.format(Constants.FILM_NOT_FOUND, id)));
	}

	@Override
	public String setFilmUnavailableAndGetType(long id) {
		Film film = findById(id);
		if(tryToSetFilmUnavailable(film)) {
			return film.getType();
		}
		return Constants.ERROR_CODE;
	}

	@Override
	public void setFilmAvailable(long id) {
		Film film = findById(id);
		film.setAvailable(true);
		repository.save(film);
	}

	private boolean tryToSetFilmUnavailable(Film film) {
		if(film.isAvailable()) {
			film.setAvailable(false);
			repository.save(film);
			return true;
		}
		log.warn(String.format(Constants.FILM_NOT_AVAILABLE, film.getId()));
		return false;
	}

}
