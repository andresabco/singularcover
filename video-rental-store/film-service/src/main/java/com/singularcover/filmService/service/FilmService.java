package com.singularcover.filmService.service;

import java.util.List;

import com.singularcover.filmService.entity.Film;

public interface FilmService {

	List<Film> findAll();

	Film findById(long id);

	List<Film> findByGenre(String genre);

	List<Film> findAvailable();

	String setFilmUnavailableAndGetType(long id);

	void setFilmAvailable(long id);

}
