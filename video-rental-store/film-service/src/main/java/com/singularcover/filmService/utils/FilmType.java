package com.singularcover.filmService.utils;

public enum FilmType {

	NEW_RELEASE("0"), REGULAR("1"), OLD("2");

	private String filmType;

	FilmType(String filmType) {
		this.filmType = filmType;
	}

	public String getFilmType() {
		return filmType;
	}

}
