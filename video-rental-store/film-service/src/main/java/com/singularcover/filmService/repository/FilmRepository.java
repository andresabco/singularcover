package com.singularcover.filmService.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.singularcover.filmService.entity.Film;

public interface FilmRepository extends JpaRepository<Film, Long> {

	public List<Film> findByGenre(String genre);

	public List<Film> findByAvailableTrue();

}
