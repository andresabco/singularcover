package com.singularcover.filmService.utils;

public class Constants {

	// Error constants
	public static final String ERROR_MESSAGE = "Error during Rest call.";
	public static final String FILM_NOT_FOUND = "Film with id %d not found.";
	public static final String FILM_NOT_AVAILABLE = "Film with id %d not available.";
	public static final String ERROR_CODE = "-1";

}
