package com.singularcover.filmService.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.singularcover.filmService.entity.Film;
import com.singularcover.filmService.service.FilmService;
import com.singularcover.filmService.utils.Utils;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class FilmController {

	private final FilmService filmService;

	@GetMapping("/films")
	public ResponseEntity<List<Film>> findAll() {
		return Utils.createOkResponseEntity(filmService.findAll());
	}

	@GetMapping("/films/available")
	public ResponseEntity<List<Film>> findAvailable() {
		return Utils.createOkResponseEntity(filmService.findAvailable());
	}

	@GetMapping("/films/genre/{genre}")
	public ResponseEntity<List<Film>> findByGenre(@PathVariable String genre) {
		return Utils.createOkResponseEntity(filmService.findByGenre(genre));
	}

	@GetMapping("/films/{id}")
	public ResponseEntity<Film> findById(@PathVariable long id) {
		return Utils.createOkResponseEntity(filmService.findById(id));
	}

	@GetMapping("/films/setFilmUnavailable/{id}")
	public ResponseEntity<String> setFilmUnavailableAndGetType(@PathVariable long id) {
		return Utils.createOkResponseEntity(filmService.setFilmUnavailableAndGetType(id));
	}

	@PostMapping("/films/setFilmAvailable/{id}")
	public ResponseEntity<Void> setFilmAvailable(@PathVariable long id) {
		filmService.setFilmAvailable(id);
		return Utils.createEmptyOkResponseEntity();
	}

}
